<?php
/**
 * @version     1.0.0
 * @package     mod_dzguideauthor
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <dev@dzdev.com> - dzdev.com
 */
 
// no direct access
defined('_JEXEC') or die;
?>
<div class="guideauthor-module<?php echo $moduleclass_sfx; ?>">
    <table class="author-list">
        <tbody>
            <?php foreach ($authors as $author) : ?>
            <tr>
                <td width="90px">
                    <?= $author->profileurl ? "<a href='$author->profileurl' target='_blank'>" : null ?>
                    <img src="<?= $author->avatar ? $author->avatar : 'http://placehold.it/70x70&text=PlayDota'; ?>" width="70" class="img-thumb" />
                    <?= $author->profileurl ? '</a>' : null; ?>
                </td>
                <td>
                    <h3 class="t1"><?= $author->name; ?></h3>
                    <div class="small">
                        <i class="fa fa-book"></i> <?= $author->guide_count; ?> hướng dẫn
                        -
                        <?= $author->upvote_count; ?> <i class="fa fa-star"></i>
                        -
                        <?= $author->downvote_count; ?> <i class="fa fa-thumbs-down"></i>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tr>
    </table>
</div>
