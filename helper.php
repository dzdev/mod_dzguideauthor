<?php
/**
 * @version     1.0.0
 * @package     mod_dzguideauthor
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <dev@dzdev.com> - dzdev.com
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_dzguide/models');

abstract class modDZGuideAuthorHelper
{
    public static function getList($params)
    {
        $model = JModelLegacy::getInstance('Authors', 'DZGuideModel', array('ignore_request' => true));
        $model->setState('list.limit', $params->get('display_num', 5));
        $model->setState('list.ordering', $params->get('filter_order', 'upvote_count'));
        $model->setState('list.direction', $params->get('filter_order_Dir', 'desc'));
        $model->setState('filter.exclude_id', $params->get('exclude_id'));
        
        return $model->getItems();
    }
}
