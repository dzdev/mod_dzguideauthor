<?php
/**
 * @version     1.0.0
 * @package     mod_dzguideauthor
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <dev@dzdev.com> - dzdev.com
 */
 
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$authors = modDZGuideAuthorHelper::getList($params);

// Display template
require JModuleHelper::getLayoutPath('mod_dzguideauthor', $params->get('layout', 'default'));
